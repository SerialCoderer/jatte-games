package com.jatte.heartsgame;

import com.jatte.GameKey;
import com.jatte.api.cards.Card;
import com.jatte.api.cards.Suits;
import com.jatte.client.Move;
import com.jatte.heartsgame.States.Dealing;
import com.jatte.heartsgame.States.PassingLeft;
import com.jatte.heartsgame.States.PlayHands;
import com.jatte.service.IllegalMoveException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;


@Ignore
public class HeartsGameTest {

	private GameKey key = new GameKey(2, HeartsGame.class.toString());
	private HeartsGame game = new HeartsGame(key);

	private Card<Suits, Integer> twoOfClubs = new Card(Suits.CLUBS, new Integer(0), "2");
	private Card<Suits, Integer> threeOfClubs =	new Card(Suits.CLUBS, new Integer(3), "3");
	private Card<Suits, Integer> fourOfClubs = new Card(Suits.CLUBS, new Integer(4), "4");
	private Card<Suits, Integer> fiveOfClubs = new Card<Suits, Integer>(Suits.CLUBS, new Integer(5), "5");

	private Card<Suits, Integer> sevenOfDiamonds = new Card(Suits.DIAMONDS, new Integer(7), "7");
	private Card<Suits, Integer> eightOfDiamonds =	new Card(Suits.DIAMONDS, new Integer(8), "8");
	private Card<Suits, Integer> nineOfDiamonds= new Card(Suits.DIAMONDS, new Integer(9), "9");


	private Card<Suits, Integer> sevenOfHearts = new Card(Suits.HEARTS, new Integer(7), "7");
	private Card<Suits, Integer> eightOfHearts =	new Card(Suits.HEARTS, new Integer(8), "8");
	private Card<Suits, Integer> nineOfHearts = new Card(Suits.HEARTS, new Integer(9), "9");


	private Card<Suits, Integer> sevenOfSpades = new Card(Suits.SPADES, new Integer(7), "7");
	private Card<Suits, Integer> eightOfSpades =	new Card(Suits.SPADES, new Integer(8), "8");
	private Card<Suits, Integer> nineOfSpades = new Card(Suits.SPADES, new Integer(9), "9");

	@Before
	public void start(){
		game.addPlayer("tester1");
		game.addPlayer("tester2");
		game.addPlayer("tester3");
		game.addPlayer("tester4");
	}


	@Test
	public void testFullHeartGame(){
		

	}

	@Test
	public void heartGame_Should_StartWhenAllPlayersHaveJoined(){
		GameKey key = new GameKey(2, HeartsGame.class.toString());
		HeartsGame game = new HeartsGame(key);
	}


	@Test
	public void gameShouldBeFullWhen4PlayersHaveJoinedWithUsersNames(){

		GameKey key = new GameKey(2, HeartsGame.class.toString());
		HeartsGame game = new HeartsGame(key);

		Assert.assertFalse(game.isFull());

		game.addPlayer("tester1");
		Assert.assertFalse(game.isFull());

		game.addPlayer("tester2");
		Assert.assertFalse(game.isFull());

		game.addPlayer("tester3");
		Assert.assertFalse(game.isFull());

		game.addPlayer("tester4");
		Assert.assertEquals(true, game.isFull());
	}

	@Test(expected=IllegalMoveException.class)
	public void should_notAllowMoveIfNot3Cards_when_PassingLeft() throws IllegalMoveException {

		List<Card<Suits, Integer>> cards = new ArrayList<>();
		cards.add(twoOfClubs);
		cards.add(threeOfClubs);
		cards.add(fourOfClubs);
		cards.add(fiveOfClubs);

		HeartsGamePlayer player1 = game.getPlayer("tester1");
		player1.addPlayerCards(cards);
		Move<HeartsGameMoveType, List<Card<Suits, Integer>>> move = new Move<>(HeartsGameMoveType.PASS_LEFT, cards);

		Assert.assertEquals(4, cards.size());
		game.setGameState(new PassingLeft(game));
		Assert.assertEquals(PassingLeft.class, game.getGameState().getClass());
//		game.move(move, player1.getUserName());

		cards.remove(fiveOfClubs);
		cards.remove(fourOfClubs);

		Assert.assertEquals(2, player1.getPlayerCards().size());
//		game.move(move, player1.getUserName());


		Assert.assertNull(player1.getPlayerCards());
//		game.move(move, player1.getUserName());
	}




	@Test(expected=IllegalMoveException.class)
	public void should_throwIllegalMoveEception_when_player1HasMovedAlready() throws IllegalMoveException {

		List<Card<Suits, Integer>> cards = new ArrayList<>();
		cards.add(twoOfClubs);
		cards.add(threeOfClubs);
		cards.add(fourOfClubs);

		HeartsGamePlayer player1 = game.getPlayer("tester1");
		HeartsGamePlayer player2 = game.getPlayer("tester2");
		player1.addPlayerCards(cards);

		Assert.assertEquals(3, player1.getPlayerCards().size());
		Assert.assertTrue(player2.getPlayerCards().isEmpty());

		Move<HeartsGameMoveType, List<Card<Suits, Integer>>> move = new Move<>(HeartsGameMoveType.PASS_LEFT, cards);

		game.setGameState(new PassingLeft(game));
		Assert.assertEquals(PassingLeft.class, game.getGameState().getClass());

//		game.move(move, player1.getUserName());
		Assert.assertTrue(player1.hasMoved());

//		game.move(move, player1.getUserName());

	}


	@Test(expected=IllegalMoveException.class)
	public void should_throwIllegalMoveEception_when_userIsNotFound() throws IllegalMoveException {

		List<Card<Suits, Integer>> cards = new ArrayList<>();
		cards.add(twoOfClubs);
		cards.add(threeOfClubs);
		cards.add(fourOfClubs);

		HeartsGamePlayer player1 = game.getPlayer("tester1");
		player1.addPlayerCards(cards);

		Move<HeartsGameMoveType, List<Card<Suits, Integer>>> move = new Move<>(HeartsGameMoveType.PASS_LEFT, cards);
		game.setGameState(new PassingLeft(game));
//		game.move(move, "");
	}


	@Test
	public void onStartShouldDeal13CardsToEachPlayer(){
		GameKey key = new GameKey(2, HeartsGame.class.toString());
		HeartsGame game = new HeartsGame(key);

		game.addPlayer("tester1");
		game.addPlayer("tester2");
		game.addPlayer("tester3");
		game.addPlayer("tester4");

		game.startGame();

		Assert.assertEquals(13, game.getPlayer("tester1").getPlayerCards().size());
		Assert.assertEquals(13, game.getPlayer("tester2").getPlayerCards().size());
		Assert.assertEquals(13, game.getPlayer("tester3").getPlayerCards().size());
		Assert.assertEquals(13, game.getPlayer("tester4").getPlayerCards().size());
	}


	@Test
	public void should_notAddPassedCardsToPlayer_when_playerHasNotPassedHisCards() throws IllegalMoveException{
		HeartsGamePlayer player1 = game.getPlayer("tester1");
		HeartsGamePlayer player2 = game.getPlayer("tester2");


		List<Card<Suits, Integer>> player1Cards = new ArrayList<>();
		player1Cards.add(twoOfClubs);
		player1Cards.add(threeOfClubs);
		player1Cards.add(fourOfClubs);

		player1.addPlayerCards(player1Cards);

		Assert.assertEquals(0, player2.getPlayerCards().size());
		Assert.assertEquals(false, player1.hasMoved());
		Move<HeartsGameMoveType, List<Card<Suits, Integer>>> move = new Move<>(HeartsGameMoveType.PASS_LEFT, player1Cards);
		game.setGameState(new PassingLeft(game));
//		game.move(move, "tester1");

		Assert.assertEquals(0, player2.getPlayerCards().size());
		List<Card<Suits, Integer>> cachedCard = game.getCachedCards("tester2");
		Assert.assertTrue(cachedCard.contains(twoOfClubs));
		Assert.assertTrue(cachedCard.contains(threeOfClubs));
		Assert.assertTrue(cachedCard.contains(fourOfClubs));
	}



	@Test
	public void should_updatePlayerHandWithCachedCards_when_CardsWerePassedToHimBeforeHeMoved() throws IllegalMoveException{
		HeartsGamePlayer player1 = game.getPlayer("tester1");
		HeartsGamePlayer player2 = game.getPlayer("tester2");


		List<Card<Suits, Integer>> player1Cards = new ArrayList<>();
		player1Cards.add(twoOfClubs);
		player1Cards.add(threeOfClubs);
		player1Cards.add(fourOfClubs);

		player1.addPlayerCards(player1Cards);

		Assert.assertEquals(0, player2.getPlayerCards().size());
		Assert.assertEquals(false, player2.hasMoved());

		Move<HeartsGameMoveType, List<Card<Suits, Integer>>> move = new Move<>(HeartsGameMoveType.PASS_LEFT, player1Cards);
		game.setGameState(new PassingLeft(game));
//		game.move(move, "tester1");

		Assert.assertEquals(0, player2.getPlayerCards().size());

		List<Card<Suits, Integer>> player2Cards = new ArrayList<>();
		player2Cards.add(sevenOfDiamonds);
		player2Cards.add(eightOfDiamonds);
		player2Cards.add(nineOfDiamonds);

		player2.addPlayerCards(player2Cards);

		move = new Move<>(HeartsGameMoveType.PASS_LEFT, player2Cards);
//		game.move(move, "tester2");

		List<Card<Suits, Integer>> player2Hand = player2.getPlayerCards();
		Assert.assertTrue(player2Hand.contains(twoOfClubs));
		Assert.assertTrue(player2Hand.contains(threeOfClubs));
		Assert.assertTrue(player2Hand.contains(fourOfClubs));

	}


	@Test
	public void playerWith2OfclubsShouldBeFirstToMove(){

	}

	@Test
	public void shouldAssignEachPLayerUniquePlayerId() throws Exception {

		GameKey key = new GameKey(2, HeartsGame.class.toString());
		HeartsGame game = new HeartsGame(key);

		game.addPlayer("tester1");
		Assert.assertEquals(Integer.valueOf(1), game.getPlayerId("tester1"));

		game.addPlayer("tester2");
		Assert.assertEquals(Integer.valueOf(2), game.getPlayerId("tester2"));

		game.addPlayer("tester3");
		Assert.assertEquals(Integer.valueOf(3), game.getPlayerId("tester3"));

		game.addPlayer("tester4");
		Assert.assertEquals(Integer.valueOf(4), game.getPlayerId("tester4"));

	}


	@Test
	public void firstMoveAfterDeal_shouldPassLeftForRoundMod4() {

		Assert.assertTrue(Dealing.class == game.getGameState().getClass());

		game.setCurrentRoundNumber(0);
		game.startGame();

		Assert.assertEquals(PassingLeft.class, game.getGameState().getClass());
	}


	@Test
	public void should_moveGameToPlayCardsState_when_allPlayersHavePassedLeft() throws IllegalMoveException{

		HeartsGamePlayer player1 = game.getPlayer("tester1");
		List<Card<Suits, Integer>> player1Cards = new ArrayList<>();
		player1Cards.add(twoOfClubs);
		player1Cards.add(threeOfClubs);
		player1Cards.add(fourOfClubs);
		player1.addPlayerCards(player1Cards);


		Move<HeartsGameMoveType, List<Card<Suits, Integer>>> move = new Move<>(HeartsGameMoveType.PASS_LEFT, player1Cards);
		game.setGameState(new PassingLeft(game));
//		game.move(move, "tester1");

		Assert.assertEquals(PassingLeft.class, game.getGameState().getClass());

		HeartsGamePlayer player2 = game.getPlayer("tester2");
		List<Card<Suits, Integer>> player2Cards = new ArrayList<>();
		player2Cards.add(twoOfClubs);
		player2Cards.add(threeOfClubs);
		player2Cards.add(fourOfClubs);
		player2.addPlayerCards(player2Cards);

		Assert.assertEquals(PassingLeft.class, game.getGameState().getClass());
		move = new Move<>(HeartsGameMoveType.PASS_LEFT, player2Cards);
//		game.move(move, "tester2");

		HeartsGamePlayer player3 = game.getPlayer("tester3");
		List<Card<Suits, Integer>> player3Cards = new ArrayList<>();
		player3Cards.add(twoOfClubs);
		player3Cards.add(threeOfClubs);
		player3Cards.add(fourOfClubs);

		Assert.assertEquals(PassingLeft.class, game.getGameState().getClass());
		player3.addPlayerCards(player3Cards);
		move = new Move<>(HeartsGameMoveType.PASS_LEFT, player3Cards);
//		game.move(move, "tester3");

		HeartsGamePlayer player4 = game.getPlayer("tester4");
		List<Card<Suits, Integer>> player4Cards = new ArrayList<>();
		player4Cards.add(twoOfClubs);
		player4Cards.add(threeOfClubs);
		player4Cards.add(fourOfClubs);
		player4.addPlayerCards(player4Cards);

		Assert.assertEquals(PassingLeft.class, game.getGameState().getClass());

		move = new Move<>(HeartsGameMoveType.PASS_LEFT, player4Cards);
//		game.move(move, "tester4");

		Assert.assertEquals(PlayHands.class, game.getGameState().getClass());
	}


	@Test
	public void should_findPlayerWith2OfClubs_when_AtBeginningAtPlayHandsRound(){


	}

	@Test(expected = IllegalMoveException.class)
	public void should_throwAnException_when_moveStateDoesNotMatchGameState() throws IllegalMoveException{


		HeartsGamePlayer player1 = game.getPlayer("tester1");
		List<Card<Suits, Integer>> player1Cards = new ArrayList<>();
		player1Cards.add(twoOfClubs);
		player1.addPlayerCards(player1Cards);


		Move<HeartsGameMoveType, List<Card<Suits, Integer>>> move = new Move<>(HeartsGameMoveType.PASS_LEFT, player1Cards);
		game.setGameState(new PlayHands(game));

//		game.move(move);

	}
}


