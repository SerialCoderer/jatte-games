package com.jatte.heartsgame.States;


import com.jatte.IllegalMoveException;
import com.jatte.api.cards.Card;
import com.jatte.api.cards.Suits;
import com.jatte.client.Move;
import com.jatte.heartsgame.HeartsDeckOfCards;
import com.jatte.heartsgame.HeartsGame;
import com.jatte.heartsgame.HeartsGameMoveType;
import com.jatte.heartsgame.HeartsGamePlayer;
import org.junit.*;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static com.jatte.heartsgame.HeartsDeckOfCards.*;

/**
 * Created by jovaughnlockridge1 on 10/13/16.
 */
@Ignore
public class PlayHandsTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();


    private HeartsGame game;


    @Before
    public void start(){
        game = new HeartsGame();
        game.addPlayer("tester1");
        game.addPlayer("tester2");
        game.addPlayer("tester3");
        game.addPlayer("tester4");
    }

    @Test
    public void should_bePlayerWithTwoOfClubsMoving_when_firstMoveOfPlayHands() throws IllegalMoveException {

        HeartsGamePlayer player1 = game.getPlayer("tester1");
        List<Card<Suits, Integer>> player1Cards = new ArrayList<>();
        player1Cards.add(twoOfClubs);
        player1.addPlayerCards(player1Cards);


        Move<HeartsGameMoveType, List<Card<Suits, Integer>>> move = new Move<>(HeartsGameMoveType.PLAY_HANDS, player1Cards);
        game.setGameState(new PlayHands(game));
        game.setWhoMove(player1);
//        game.move(move, "tester1");


        Assert.assertFalse(player1.getPlayerCards().contains(twoOfClubs));
//        Map<String, Card<Suits, Integer>> playerMoves = game.getPlayerMoves();
//        Assert.assertEquals(twoOfClubs, playerMoves.get("tester1"));
        Assert.assertTrue(player1.hasMoved());
    }

    @Test
    public void should_notAllowNonTwoOfClubsPlayerPlayerMove_when_firstMoveOfPlayHands() throws IllegalMoveException{

        HeartsGamePlayer player1 = game.getPlayer("tester1");
        player1.addPlayerCard(HeartsDeckOfCards.twoOfClubs);
        List<Card<Suits, Integer>> player1Cards = new ArrayList<>();
        player1Cards.add(threeOfClubs);
        player1.addPlayerCards(player1Cards);

        Move<HeartsGameMoveType, List<Card<Suits, Integer>>> move = new Move<>(HeartsGameMoveType.PLAY_HANDS, player1Cards);
        game.setGameState(new PlayHands(game));

        thrown.expect(IllegalMoveException.class);
        thrown.expectMessage("First move must be 2 of Clubs");
        game.setWhoMove(player1);
//        game.move(move, "tester1");

    }




    @Test
    public void should_notAllowMoveOfHeartSuitToBeFirstPlayed_when_iceHasNotBeenBrokenAndPlayerHasOtherSuits() throws IllegalMoveException{

        firstTwoOfClubsBook();
        game.postMove();

        HeartsGamePlayer player = game.getPlayer("tester3");
        List<Card<Suits, Integer>> playerCards = new ArrayList<>();
        playerCards.add(sevenOfHearts);

        player.getPlayerCards().add(sevenOfDiamonds);
        player.addPlayerCards(playerCards);

        Move<HeartsGameMoveType, List<Card<Suits, Integer>>> move = new Move<>(HeartsGameMoveType.PLAY_HANDS, playerCards);
        thrown.expect(IllegalMoveException.class);
        thrown.expectMessage("Can't play hearts ice has not been broken.");
//         game.move(move, "tester3");

    }

    @Test
    public void should_AllowMoveOfHeartSuitToBePlayed_when_playerDoesNotHaveMatchingLeadSuit() throws IllegalMoveException{

        firstTwoOfClubsBook();
        game.postMove();

        HeartsGamePlayer player = game.getPlayer("tester3");
        List<Card<Suits, Integer>> playerCards = new ArrayList<>();
        playerCards.add(sevenOfClubs);
        player.addPlayerCards(playerCards);

        Move<HeartsGameMoveType, List<Card<Suits, Integer>>> move = new Move<>(HeartsGameMoveType.PLAY_HANDS, playerCards);
//        game.move(move, "tester3");

        HeartsGamePlayer heartsPlayer = game.getPlayer("tester4");
        List<Card<Suits, Integer>> heartsPlayerCards = new ArrayList<>();
        heartsPlayerCards.add(sevenOfHearts);
        heartsPlayer.addPlayerCards(heartsPlayerCards);
        heartsPlayer.addPlayerCard(sevenOfDiamonds);

        Move<HeartsGameMoveType, List<Card<Suits, Integer>>> heartsPlayerMove = new Move<>(HeartsGameMoveType.PLAY_HANDS, heartsPlayerCards);
//        game.move(heartsPlayerMove, "tester4");

//        Assert.assertTrue(game.getPlayerMoves().values().contains(sevenOfHearts));

    }

    @Test
    public void should_AllowHeartsToLeadBook_when_iceHasBeenBroken(){

    }

    @Test
    public void should_breakIceWhenQueenOfSpadesIsPlayed_when_noHeartsHaveBeenPlayed() throws IllegalMoveException{

        HeartsGamePlayer player1 = game.getPlayer("tester1");
        game.setEndOfGameScore(100);

        List<Card<Suits, Integer>> player1Cards = new ArrayList<>();
        player1Cards.add(HeartsDeckOfCards.twoOfClubs);
        player1Cards.add(HeartsDeckOfCards.eightOfDiamonds);
        player1Cards.add(HeartsDeckOfCards.nineOfDiamonds);

        player1.addPlayerCards(player1Cards);

        Move<HeartsGameMoveType, List<Card<Suits, Integer>>> move = new Move<>(HeartsGameMoveType.PLAY_HANDS, Arrays.asList(player1Cards.get(0)));
        game.setGameState(new PlayHands(game));
        game.setWhoMove(player1);
//        game.move(move, "tester1");

        HeartsGamePlayer player2 = game.getPlayer("tester2");
        List<Card<Suits, Integer>> player2Cards = new ArrayList<>();
        player2Cards.add(HeartsDeckOfCards.queenOfSpades);
        player2Cards.add(HeartsDeckOfCards.threeOfHearts);
        player2Cards.add(HeartsDeckOfCards.fiveOfDiamonds);
        player2.addPlayerCards(player2Cards);
        move = new Move<>(HeartsGameMoveType.PLAY_HANDS,  Arrays.asList(player2Cards.get(0)));
//        game.move(move, "tester2");


        HeartsGamePlayer player3 = game.getPlayer("tester3");
        List<Card<Suits, Integer>> player3Cards = new ArrayList<>();
        player3Cards.add(HeartsDeckOfCards.fiveOfClubs);
        player3Cards.add(HeartsDeckOfCards.sixOfClubs);
        player3Cards.add(HeartsDeckOfCards.sevenOfClubs);
        player3.addPlayerCards(player3Cards);
        move = new Move<>(HeartsGameMoveType.PLAY_HANDS, Arrays.asList(player3Cards.get(0)));
//        game.move(move, "tester3");

        HeartsGamePlayer player4 = game.getPlayer("tester4");
        List<Card<Suits, Integer>> player4Cards = new ArrayList<>();
        player4Cards.add(HeartsDeckOfCards.fourOfClubs);
        player4Cards.add(HeartsDeckOfCards.jackOfDiamonds);
        player4Cards.add(HeartsDeckOfCards.aceOfDiamonds);
        player4.addPlayerCards(player4Cards);
        move = new Move<>(HeartsGameMoveType.PLAY_HANDS, Arrays.asList(player4Cards.get(0)));
//        game.move(move, "tester4");
        game.postMove();
        Assert.assertTrue(game.getRoundBooks("tester3").contains(HeartsDeckOfCards.queenOfSpades));

        Assert.assertEquals("tester3", game.getWhoMove().getUserName());
//        Assert.assertTrue(game.getPlayerMoves().isEmpty());


        move = new Move<>(HeartsGameMoveType.PLAY_HANDS, Arrays.asList(player3Cards.get(1)));
//        game.move(move, "tester3");
        move = new Move<>(HeartsGameMoveType.PLAY_HANDS, Arrays.asList(player4Cards.get(1)));
//        game.move(move, "tester4");
        move = new Move<>(HeartsGameMoveType.PLAY_HANDS, Arrays.asList(player1Cards.get(1)));
//        game.move(move, "tester1");
        move = new Move<>(HeartsGameMoveType.PLAY_HANDS,  Arrays.asList(player2Cards.get(1)));
//        game.move(move, "tester2");
//        Assert.assertTrue(game.getPlayerMoves().values().contains(HeartsDeckOfCards.threeOfHearts));
        game.postMove();

        move = new Move<>(HeartsGameMoveType.PLAY_HANDS, Arrays.asList(player3Cards.get(2)));
//        game.move(move, "tester3");
        move = new Move<>(HeartsGameMoveType.PLAY_HANDS, Arrays.asList(player4Cards.get(2)));
//        game.move(move, "tester4");
        move = new Move<>(HeartsGameMoveType.PLAY_HANDS, Arrays.asList(player1Cards.get(2)));
//        game.move(move, "tester1");
        move = new Move<>(HeartsGameMoveType.PLAY_HANDS,  Arrays.asList(player2Cards.get(2)));
//        game.move(move, "tester2");
        game.postMove();

        Assert.assertEquals(game.PASSING_RIGHT, game.getGameState());
    }


    @Test
    public void should_AllowNonLeadingSuitToBePlayed_when_playerDoesNotHaveLeadingSuitInHand() throws IllegalMoveException {

        HeartsGamePlayer player1 = game.getPlayer("tester1");

        List<Card<Suits, Integer>> player1Cards = new ArrayList<>();
        player1Cards.add(twoOfClubs);
        player1.addPlayerCards(player1Cards);

        Move<HeartsGameMoveType, List<Card<Suits, Integer>>> move = new Move<>(HeartsGameMoveType.PLAY_HANDS, player1Cards);
        game.setGameState(new PlayHands(game));
        game.setWhoMove(player1);
//        game.move(move, "tester1");

        HeartsGamePlayer player2 = game.getPlayer("tester2");
        List<Card<Suits, Integer>> player2Cards = new ArrayList<>();
        player2Cards.add(HeartsDeckOfCards.twoOfSpades);
        player2.addPlayerCards(player2Cards);
        move = new Move<>(HeartsGameMoveType.PLAY_HANDS, player2Cards);
//        game.move(move, "tester2");

        HeartsGamePlayer player3 = game.getPlayer("tester3");
        List<Card<Suits, Integer>> player3Cards = new ArrayList<>();
        player3Cards.add(fiveOfClubs);
        player3.addPlayerCards(player3Cards);
        move = new Move<>(HeartsGameMoveType.PLAY_HANDS, player3Cards);
//        game.move(move, "tester3");

        HeartsGamePlayer player4 = game.getPlayer("tester4");
        List<Card<Suits, Integer>> player4Cards = new ArrayList<>();
        player4Cards.add(fourOfClubs);
        player4.addPlayerCards(player4Cards);
        move = new Move<>(HeartsGameMoveType.PLAY_HANDS, player4Cards);
//        game.move(move, "tester4");
        game.postMove();

        Assert.assertEquals("tester3", game.getWhoMove().getUserName());
//        Assert.assertTrue(game.getPlayerMoves().isEmpty());

    }

    @Test
    public void should_resetEachPlayerMoveStatus_when_AtEndOfBook() throws IllegalMoveException{

        firstTwoOfClubsBook();
        game.postMove();

        HeartsGamePlayer player1 = game.getPlayer("tester1");
        HeartsGamePlayer player2 = game.getPlayer("tester2");
        HeartsGamePlayer player3 = game.getPlayer("tester3");
        HeartsGamePlayer player4 = game.getPlayer("tester4");

        Assert.assertFalse(player1.hasMoved());
        Assert.assertFalse(player2.hasMoved());
        Assert.assertFalse(player3.hasMoved());
        Assert.assertFalse(player4.hasMoved());
    }

    @Test
    public void should_updateRound_when_AllPlayersPLayedTheirCard() throws IllegalMoveException{
        HeartsGamePlayer player1 = game.getPlayer("tester1");
        game.setWhoMove(player1);
        firstTwoOfClubsBook();
        game.postMove();

        Assert.assertTrue(game.getWhoMove().getUserName().equals("tester3"));
//        Assert.assertTrue(game.getPlayerMoves().isEmpty());

    }

    @Test
    public void should_calculateScores_when_endOfRound() throws IllegalMoveException {
        should_breakIceWhenQueenOfSpadesIsPlayed_when_noHeartsHaveBeenPlayed();
        game.postMove();

    }


    @Test
    public void should_endGame_when_aPlayerHasReach100Points() throws IllegalMoveException{

        HeartsGamePlayer player1 = game.getPlayer("tester1");
        player1.setPlayerScore(0);


        List<Card<Suits, Integer>> player1Cards = new ArrayList<>();
        player1Cards.add(HeartsDeckOfCards.twoOfClubs);
        player1.addPlayerCards(player1Cards);

        Move<HeartsGameMoveType, List<Card<Suits, Integer>>> move = new Move<>(HeartsGameMoveType.PLAY_HANDS, Arrays.asList(player1Cards.get(0)));
        game.setGameState(new PlayHands(game));
        game.setWhoMove(player1);
//        game.move(move, "tester1");

        HeartsGamePlayer player2 = game.getPlayer("tester2");
        player2.setPlayerScore(10);
        List<Card<Suits, Integer>> player2Cards = new ArrayList<>();
        player2Cards.add(HeartsDeckOfCards.queenOfSpades);
        player2.addPlayerCards(player2Cards);
        move = new Move<>(HeartsGameMoveType.PLAY_HANDS,  Arrays.asList(player2Cards.get(0)));
//        game.move(move, "tester2");


        HeartsGamePlayer player3 = game.getPlayer("tester3");
        player3.setPlayerScore(87);
        List<Card<Suits, Integer>> player3Cards = new ArrayList<>();
        player3Cards.add(HeartsDeckOfCards.fiveOfClubs);
        player3.addPlayerCards(player3Cards);
        move = new Move<>(HeartsGameMoveType.PLAY_HANDS, Arrays.asList(player3Cards.get(0)));
//        game.move(move, "tester3");

        HeartsGamePlayer player4 = game.getPlayer("tester4");
        player4.setPlayerScore(20);
        List<Card<Suits, Integer>> player4Cards = new ArrayList<>();
        player4Cards.add(HeartsDeckOfCards.fourOfClubs);
        player4.addPlayerCards(player4Cards);
        move = new Move<>(HeartsGameMoveType.PLAY_HANDS, Arrays.asList(player4Cards.get(0)));
//        game.move(move, "tester4");
        game.postMove();

        Assert.assertEquals(player1.getUserName(), game.getGameWinner().getUserName());

    }


    @Test
    public void should_continueGame_when_endOfGameScoreReachedButPlayersHaveTiedScore() throws IllegalMoveException{

        HeartsGamePlayer player1 = game.getPlayer("tester1");
        player1.setPlayerScore(0);


        List<Card<Suits, Integer>> player1Cards = new ArrayList<>();
        player1Cards.add(HeartsDeckOfCards.twoOfClubs);
        player1.addPlayerCards(player1Cards);

        Move<HeartsGameMoveType, List<Card<Suits, Integer>>> move = new Move<>(HeartsGameMoveType.PLAY_HANDS, Arrays.asList(player1Cards.get(0)));
        game.setGameState(new PlayHands(game));
        game.setWhoMove(player1);
//        game.move(move, "tester1");

        HeartsGamePlayer player2 = game.getPlayer("tester2");
        player2.setPlayerScore(10);
        List<Card<Suits, Integer>> player2Cards = new ArrayList<>();
        player2Cards.add(HeartsDeckOfCards.queenOfSpades);
        player2.addPlayerCards(player2Cards);
        move = new Move<>(HeartsGameMoveType.PLAY_HANDS,  Arrays.asList(player2Cards.get(0)));
//        game.move(move, "tester2");


        HeartsGamePlayer player3 = game.getPlayer("tester3");
        player3.setPlayerScore(87);
        List<Card<Suits, Integer>> player3Cards = new ArrayList<>();
        player3Cards.add(HeartsDeckOfCards.fiveOfClubs);
        player3.addPlayerCards(player3Cards);
        move = new Move<>(HeartsGameMoveType.PLAY_HANDS, Arrays.asList(player3Cards.get(0)));
//        game.move(move, "tester3");

        HeartsGamePlayer player4 = game.getPlayer("tester4");
        player4.setPlayerScore(10);
        List<Card<Suits, Integer>> player4Cards = new ArrayList<>();
        player4Cards.add(HeartsDeckOfCards.fourOfClubs);
        player4.addPlayerCards(player4Cards);
        move = new Move<>(HeartsGameMoveType.PLAY_HANDS, Arrays.asList(player4Cards.get(0)));
//        game.move(move, "tester4");
        game.postMove();

        Assert.assertEquals(game.PASSING_RIGHT, game.getGameState());
        Assert.assertEquals(null, game.getGameWinner());

    }

    @Test
    public void should_playerWithAllHeartsAndQueensOfSpadesScoreIsSetToZero_and_otherPlayersPlus26_when_EndOfRound() throws IllegalMoveException{

        HeartsGamePlayer player1 = game.getPlayer("tester1");
        HeartsGamePlayer player2 = game.getPlayer("tester2");
        HeartsGamePlayer player3 = game.getPlayer("tester3");
        HeartsGamePlayer player4 = game.getPlayer("tester4");

        player1.setPlayerScore(99);
        player2.setPlayerScore(2);
        player3.setPlayerScore(3);
        player4.setPlayerScore(4);

        List<Card<Suits, Integer>> shootingMoon = Arrays.asList(
        HeartsDeckOfCards.twoOfHearts
        ,HeartsDeckOfCards.threeOfHearts
        ,HeartsDeckOfCards.fourOfHearts
        ,HeartsDeckOfCards.fiveOfHearts
        ,HeartsDeckOfCards.sixOfHearts
        , sevenOfHearts
        ,HeartsDeckOfCards.eightOfHearts
        ,HeartsDeckOfCards.nineOfHearts
        ,HeartsDeckOfCards.tenOfHearts
        ,HeartsDeckOfCards.jackOfHearts
        ,HeartsDeckOfCards.queenOfHearts
        ,HeartsDeckOfCards.kingOfHearts
        ,HeartsDeckOfCards.aceOfHearts
        ,HeartsDeckOfCards.queenOfSpades);

        game.persistRoundBooks("tester1", shootingMoon);
        firstTwoOfClubsBoolEndOfRound();
        game.postMove();

        Assert.assertEquals(0, player1.getPlayerScore());
        Assert.assertEquals(28, player2.getPlayerScore());
        Assert.assertEquals(29, player3.getPlayerScore());
        Assert.assertEquals(30, player4.getPlayerScore());

    }

    @Test
    public void should_throwAnException_whenTwoOfClubsIsNotFirstBookOfRoundPlayerInRound2() throws IllegalMoveException {

        HeartsGamePlayer player1 = game.getPlayer("tester1");

        firstTwoOfClubsBook();

        List<Card<Suits, Integer>> player1Cards = new ArrayList<>();
        player1Cards.add(HeartsDeckOfCards.aceOfClubs);
        player1.addPlayerCards(player1Cards);

        Move<HeartsGameMoveType, List<Card<Suits, Integer>>> move = new Move<>(HeartsGameMoveType.PLAY_HANDS, player1Cards);

        player1.addPlayerCard(HeartsDeckOfCards.twoOfClubs);
        game.setGameState(game.PLAY_HANDS);
        game.setWhoMove(player1);

        thrown.expect(IllegalMoveException.class);
        thrown.expectMessage("First move must be 2 of Clubs");

//        game.move(move, "tester1");



    }


    private void firstTwoOfClubsBook() throws IllegalMoveException {
        HeartsGamePlayer player1 = game.getPlayer("tester1");

        List<Card<Suits, Integer>> player1Cards = new ArrayList<>();
        player1Cards.add(twoOfClubs);
        player1Cards.add(HeartsDeckOfCards.aceOfClubs);
        player1.addPlayerCards(player1Cards);

        Move<HeartsGameMoveType, List<Card<Suits, Integer>>> move = new Move<>(HeartsGameMoveType.PLAY_HANDS, player1Cards);
        game.setGameState(game.PLAY_HANDS);
        game.setWhoMove(player1);
//        game.move(move, "tester1");

        HeartsGamePlayer player2 = game.getPlayer("tester2");
        List<Card<Suits, Integer>> player2Cards = new ArrayList<>();
        player2Cards.add(threeOfClubs);
        player2.addPlayerCards(player2Cards);
        move = new Move<>(HeartsGameMoveType.PLAY_HANDS, player2Cards);
//        game.move(move, "tester2");


        HeartsGamePlayer player3 = game.getPlayer("tester3");
        List<Card<Suits, Integer>> player3Cards = new ArrayList<>();
        player3Cards.add( fiveOfClubs );
        player3.addPlayerCards(player3Cards);

        move = new Move<>(HeartsGameMoveType.PLAY_HANDS, player3Cards);
//        game.move(move, "tester3");

        HeartsGamePlayer player4 = game.getPlayer("tester4");
        List<Card<Suits, Integer>> player4Cards = new ArrayList<>();
        player4Cards.add(fourOfClubs);
        player4.addPlayerCards(player4Cards);
        move = new Move<>(HeartsGameMoveType.PLAY_HANDS, player4Cards);
//        game.move(move, "tester4");

    }

    private void firstTwoOfClubsBoolEndOfRound() throws IllegalMoveException {
        HeartsGamePlayer player1 = game.getPlayer("tester1");

        List<Card<Suits, Integer>> player1Cards = new ArrayList<>();
        player1Cards.add(twoOfClubs);
        player1.addPlayerCards(player1Cards);

        Move<HeartsGameMoveType, List<Card<Suits, Integer>>> move = new Move<>(HeartsGameMoveType.PLAY_HANDS, player1Cards);
        game.setGameState(game.PLAY_HANDS);
        game.setWhoMove(player1);
//        game.move(move, "tester1");

        HeartsGamePlayer player2 = game.getPlayer("tester2");
        List<Card<Suits, Integer>> player2Cards = new ArrayList<>();
        player2Cards.add(threeOfClubs);
        player2.addPlayerCards(player2Cards);
        move = new Move<>(HeartsGameMoveType.PLAY_HANDS, player2Cards);
//        game.move(move, "tester2");


        HeartsGamePlayer player3 = game.getPlayer("tester3");
        List<Card<Suits, Integer>> player3Cards = new ArrayList<>();
        player3Cards.add( fiveOfClubs );
        player3.addPlayerCards(player3Cards);

        move = new Move<>(HeartsGameMoveType.PLAY_HANDS, player3Cards);
//        game.move(move, "tester3");

        HeartsGamePlayer player4 = game.getPlayer("tester4");
        List<Card<Suits, Integer>> player4Cards = new ArrayList<>();
        player4Cards.add(fourOfClubs);
        player4.addPlayerCards(player4Cards);
        move = new Move<>(HeartsGameMoveType.PLAY_HANDS, player4Cards);
//        game.move(move, "tester4");

    }
}
