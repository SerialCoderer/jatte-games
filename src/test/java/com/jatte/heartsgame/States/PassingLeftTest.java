package com.jatte.heartsgame.States;

import com.jatte.GameKey;
import com.jatte.IllegalMoveException;
import com.jatte.api.cards.Card;
import com.jatte.api.cards.Suits;
import com.jatte.client.Move;
import com.jatte.heartsgame.HeartsGame;
import com.jatte.heartsgame.HeartsGameMoveType;
import com.jatte.heartsgame.HeartsGamePlayer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Jovaughn Lockridge on 10/11/2016.
 */
@Ignore
public class PassingLeftTest {

    private GameKey key = new GameKey(2, HeartsGame.class.toString());
    private HeartsGame game = new HeartsGame(key);

    private Card<Suits, Integer> twoOfClubs = new Card(Suits.CLUBS, new Integer(0), "2");
    private Card<Suits, Integer> threeOfClubs =	new Card(Suits.CLUBS, new Integer(3), "3");
    private Card<Suits, Integer> fourOfClubs = new Card(Suits.CLUBS, new Integer(4), "4");
    private Card<Suits, Integer> fiveOfClubs = new Card(Suits.CLUBS, new Integer(5), "5");
    private Card<Suits, Integer> sixOfClubs = new Card(Suits.CLUBS, new Integer(6), "6");
    private Card<Suits, Integer> sevenOfClubs = new Card(Suits.CLUBS, new Integer(7), "7");

    @Before
    public void start(){
        game.addPlayer("tester1");
        game.addPlayer("tester2");
        game.addPlayer("tester3");
        game.addPlayer("tester4");
    }


    @Test
    public void should_player1PassesToPlayer2_when_InPassingLeftState() throws IllegalMoveException {

        HeartsGamePlayer player1 = game.getPlayer("tester1");
        List<Card<Suits, Integer>> player1Cards = new ArrayList<>();
        player1Cards.add(twoOfClubs);
        player1Cards.add(threeOfClubs);
        player1Cards.add(fourOfClubs);

        player1.addPlayerCards(player1Cards);

        HeartsGamePlayer player2 = game.getPlayer("tester2");
        player2.setHasMoved(true);
        Assert.assertEquals(0, player2.getPlayerCards().size());

        Move<HeartsGameMoveType, List<Card<Suits, Integer>>> move = new Move<>(HeartsGameMoveType.PASS_LEFT, player1Cards);
        game.setGameState(new PassingLeft(game));
//        game.move(move, "tester1");

        Assert.assertEquals(3, player2.getPlayerCards().size());
    }

    @Test
    public void should_Player2PassesToPlayer3_when_InPassingLeftState() throws IllegalMoveException{

        HeartsGamePlayer player2 = game.getPlayer("tester2");
        List<Card<Suits, Integer>> player2Cards = new ArrayList<>();
        player2Cards.add(twoOfClubs);
        player2Cards.add(threeOfClubs);
        player2Cards.add(fourOfClubs);

        player2.addPlayerCards(player2Cards);

        HeartsGamePlayer player3 = game.getPlayer("tester3");
        player3.setHasMoved(true);
        Assert.assertEquals(0, player3.getPlayerCards().size());

        Move<HeartsGameMoveType, List<Card<Suits, Integer>>> move = new Move<>(HeartsGameMoveType.PASS_LEFT, player2Cards);
        game.setGameState(new PassingLeft(game));
//        game.move(move, "tester2");

        Assert.assertEquals(3, player3.getPlayerCards().size());
    }

    @Test
    public void should_Player3PassesToPlayer4_when_InPassingLeftState() throws IllegalMoveException {

        HeartsGamePlayer player3 = game.getPlayer("tester3");
        List<Card<Suits, Integer>> player3Cards = new ArrayList<>();
        player3Cards.add(twoOfClubs);
        player3Cards.add(threeOfClubs);
        player3Cards.add(fourOfClubs);

        player3.addPlayerCards(player3Cards);

        HeartsGamePlayer player4 = game.getPlayer("tester4");
        player4.setHasMoved(true);
        Assert.assertEquals(0, player4.getPlayerCards().size());

        Move<HeartsGameMoveType, List<Card<Suits, Integer>>> move = new Move<>(HeartsGameMoveType.PASS_LEFT, player3Cards);
        game.setGameState(new PassingLeft(game));
//        game.move(move, "tester3");

        Assert.assertEquals(3, player4.getPlayerCards().size());
    }

    @Test
    public void should_Player4PassesToPlayer1_when_InPassingLeftState() throws IllegalMoveException{

        HeartsGamePlayer player4 = game.getPlayer("tester4");
        List<Card<Suits, Integer>> player4Cards = new ArrayList<>();
        player4Cards.add(twoOfClubs);
        player4Cards.add(threeOfClubs);
        player4Cards.add(fourOfClubs);

        player4.addPlayerCards(player4Cards);

        HeartsGamePlayer player1 = game.getPlayer("tester1");
        player1.setHasMoved(true);
        Assert.assertEquals(0, player1.getPlayerCards().size());

        Move<HeartsGameMoveType, List<Card<Suits, Integer>>> move = new Move<>(HeartsGameMoveType.PASS_LEFT, player4Cards);
        game.setGameState(new PassingLeft(game));
//        game.move(move, "tester4");

        Assert.assertEquals(3, player1.getPlayerCards().size());
    }


    @Test(expected=IllegalMoveException.class)
    public void should_notAllowPlayerToPassCardsToLeftMoreThanOnce_when_AlreadyPassedLeft() throws IllegalMoveException{
        HeartsGamePlayer player1 = game.getPlayer("tester1");
        List<Card<Suits, Integer>> player1Cards = new ArrayList<>();
        player1Cards.add(twoOfClubs);
        player1Cards.add(threeOfClubs);
        player1Cards.add(fourOfClubs);
        player1Cards.add(fiveOfClubs);
        player1Cards.add(sixOfClubs);
        player1Cards.add(sevenOfClubs);

        player1.addPlayerCards(player1Cards);

        HeartsGamePlayer player2 = game.getPlayer("tester2");
        player2.setHasMoved(true);
        Assert.assertEquals(0, player2.getPlayerCards().size());

        Move<HeartsGameMoveType, List<Card<Suits, Integer>>> move = new Move<>(HeartsGameMoveType.PASS_LEFT, Arrays.asList(twoOfClubs, threeOfClubs, fourOfClubs));

        game.setGameState(new PassingLeft(game));
//        game.move(move, "tester1");

        Assert.assertEquals(3, player2.getPlayerCards().size());

        move = new Move<>(HeartsGameMoveType.PASS_LEFT, Arrays.asList(fiveOfClubs, sixOfClubs, sevenOfClubs));

//        game.move(move, "tester1");
    }


    @Test
    public void should_findPlayerWithTwoOfClubs_when_AllCardsArePassedLeft() throws Exception {

        HeartsGamePlayer player1 = game.getPlayer("tester1");
        List<Card<Suits, Integer>> player1Cards = new ArrayList<>();
        player1Cards.add(twoOfClubs);
        player1Cards.add(threeOfClubs);
        player1Cards.add(fourOfClubs);
        player1.addPlayerCards(player1Cards);


        Move<HeartsGameMoveType, List<Card<Suits, Integer>>> move = new Move<>(HeartsGameMoveType.PASS_LEFT, player1Cards);
        game.setGameState(new PassingLeft(game));
//        game.move(move, "tester1");

        Assert.assertEquals(PassingLeft.class, game.getGameState().getClass());

        HeartsGamePlayer player2 = game.getPlayer("tester2");
        List<Card<Suits, Integer>> player2Cards = new ArrayList<>();
        player2Cards.add(twoOfClubs);
        player2Cards.add(threeOfClubs);
        player2Cards.add(fourOfClubs);
        player2.addPlayerCards(player2Cards);

        Assert.assertEquals(PassingLeft.class, game.getGameState().getClass());
        move = new Move<>(HeartsGameMoveType.PASS_LEFT, player2Cards);
//        game.move(move, "tester2");

        HeartsGamePlayer player3 = game.getPlayer("tester3");
        List<Card<Suits, Integer>> player3Cards = new ArrayList<>();
        player3Cards.add(twoOfClubs);
        player3Cards.add(threeOfClubs);
        player3Cards.add(fourOfClubs);

        Assert.assertEquals(PassingLeft.class, game.getGameState().getClass());
        player3.addPlayerCards(player3Cards);
        move = new Move<>(HeartsGameMoveType.PASS_LEFT, player3Cards);
//        game.move(move, "tester3");

        HeartsGamePlayer player4 = game.getPlayer("tester4");
        List<Card<Suits, Integer>> player4Cards = new ArrayList<>();
        player4Cards.add(twoOfClubs);
        player4Cards.add(threeOfClubs);
        player4Cards.add(fourOfClubs);
        player4.addPlayerCards(player4Cards);

        Assert.assertEquals(PassingLeft.class, game.getGameState().getClass());

        move = new Move<>(HeartsGameMoveType.PASS_LEFT, player4Cards);
//        game.move(move, "tester4");

        Assert.assertEquals(PlayHands.class, game.getGameState().getClass());

        Assert.assertFalse(player1.hasMoved());
        Assert.assertFalse(player2.hasMoved());
        Assert.assertFalse(player3.hasMoved());
        Assert.assertFalse(player4.hasMoved());
    }




}
