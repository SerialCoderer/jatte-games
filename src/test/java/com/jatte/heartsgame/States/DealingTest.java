package com.jatte.heartsgame.States;

import com.jatte.Player;
import com.jatte.heartsgame.HeartsGame;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.UUID;

/**
 * Created by jovaughnlockridge1 on 9/14/16.
 */
@Ignore
public class DealingTest {

    private Player player1;
    private Player player2;
    private Player player3;
    private Player player4;

    @Before
    public void start(){

        UUID uuid1 = UUID.randomUUID();
        UUID uuid2 = UUID.randomUUID();
        UUID uuid3 = UUID.randomUUID();
        UUID uuid4 = UUID.randomUUID();

        player1 = new Player(uuid1, "tester1");
        player2 = new Player(uuid2, "tester2");
        player3 = new Player(uuid3, "tester3");
        player4 = new Player(uuid4, "tester4");
    }

    @Test
    public void shouldSetGameStateToPassLeftWhenRoundMod4EqualZero(){

        HeartsGame g = new HeartsGame();
        g.setCurrentRoundNumber(0);
        Dealing dealingState = new Dealing(g);
        dealingState.dealingComplete();

        Assert.assertEquals(PassingLeft.class, g.getGameState().getClass());
    }

    @Test
    public void shouldAllowOnlyThreeCardsToPassLeft(){

        HeartsGame heartGame = new HeartsGame();
        heartGame.addPlayer(player1.getUserName());
        heartGame.addPlayer(player2.getUserName());
        heartGame.addPlayer(player3.getUserName());
        heartGame.addPlayer(player4.getUserName());
        heartGame.startGame();

        heartGame.setCurrentRoundNumber(0);
        Dealing dealingState = new Dealing(heartGame);


        Assert.assertEquals(PassingLeft.class, heartGame.getGameState().getClass());

    }
}
