package com.jatte.heartsgame;

/**
 * *******************************************************************
 * Class: DeckOfCards Description: this class extends an ArrayList creates a 52
 * deck of cards of all the card suits
 *
 * @author Jovaughn Lockridge jovaughn@jovaapps.com Copyright
 * @ 2006
 * <p/>
 * ******************************************************************
 */


import com.jatte.api.cards.Card;
import com.jatte.api.cards.EmptyDeckException;
import com.jatte.api.cards.Suits;

import java.io.Serializable;
import java.util.*;

public class HeartsDeckOfCards implements Serializable, Cloneable {

    public static final Card twoOfDiamonds = new Card(Suits.DIAMONDS, new Integer(0), "2", 1);
    public static final Card threeOfDiamonds = new Card(Suits.DIAMONDS, new Integer(0), "3", 2);
    public static final Card fourOfDiamonds = new Card(Suits.DIAMONDS, new Integer(0), "4", 3);
    public static final Card fiveOfDiamonds = new Card(Suits.DIAMONDS, new Integer(0), "5", 4);
    public static final Card sixOfDiamonds = new Card(Suits.DIAMONDS, new Integer(0), "6", 5);
    public static final Card sevenOfDiamonds = new Card(Suits.DIAMONDS, new Integer(0), "7", 6);
    public static final Card eightOfDiamonds = new Card(Suits.DIAMONDS, new Integer(0), "8", 7);
    public static final Card nineOfDiamonds = new Card(Suits.DIAMONDS, new Integer(0), "9", 8);
    public static final Card tenOfDiamonds = new Card(Suits.DIAMONDS, new Integer(0), "10", 9);
    public static final Card jackOfDiamonds = new Card(Suits.DIAMONDS, new Integer(0), "Jack", 10);
    public static final Card queenOfDiamonds = new Card(Suits.DIAMONDS, new Integer(0), "Queen", 11);
    public static final Card kingOfDiamonds = new Card(Suits.DIAMONDS, new Integer(0), "King", 12);
    public static final Card aceOfDiamonds = new Card(Suits.DIAMONDS, new Integer(0), "Ace", 13);

    public static final Card twoOfClubs = new Card(Suits.CLUBS, new Integer(0), "2", 1);
    public static final Card threeOfClubs = new Card(Suits.CLUBS, new Integer(0), "3", 2);
    public static final Card fourOfClubs = new Card(Suits.CLUBS, new Integer(0), "4", 3);
    public static final Card fiveOfClubs = new Card(Suits.CLUBS, new Integer(0), "5", 4);
    public static final Card sixOfClubs = new Card(Suits.CLUBS, new Integer(0), "6", 5);
    public static final Card sevenOfClubs = new Card(Suits.CLUBS, new Integer(0), "7", 6);
    public static final Card eightOfClubs = new Card(Suits.CLUBS, new Integer(0), "8", 7);
    public static final Card nineOfClubs = new Card(Suits.CLUBS, new Integer(0), "9", 8);
    public static final Card tenOfClubs = new Card(Suits.CLUBS, new Integer(0), "10", 9);
    public static final Card jackOfClubs = new Card(Suits.CLUBS, new Integer(0), "Jack", 10);
    public static final Card queenOfClubs = new Card(Suits.CLUBS, new Integer(0), "Queen", 11);
    public static final Card kingOfClubs = new Card(Suits.CLUBS, new Integer(0), "King", 12);
    public static final Card aceOfClubs = new Card(Suits.CLUBS, new Integer(0), "Ace", 13);

    public static final Card twoOfHearts = new Card(Suits.HEARTS, new Integer(1), "2", 1);
    public static final Card threeOfHearts = new Card(Suits.HEARTS, new Integer(1), "3", 2);
    public static final Card fourOfHearts = new Card(Suits.HEARTS, new Integer(1), "4", 3);
    public static final Card fiveOfHearts = new Card(Suits.HEARTS, new Integer(1), "5", 4);
    public static final Card sixOfHearts = new Card(Suits.HEARTS, new Integer(1), "6", 5);
    public static final Card sevenOfHearts = new Card(Suits.HEARTS, new Integer(1), "7", 6);
    public static final Card eightOfHearts = new Card(Suits.HEARTS, new Integer(1), "8", 7);
    public static final Card nineOfHearts = new Card(Suits.HEARTS, new Integer(1), "9", 8);
    public static final Card tenOfHearts = new Card(Suits.HEARTS, new Integer(1), "10", 9);
    public static final Card jackOfHearts = new Card(Suits.HEARTS, new Integer(1), "Jack", 10);
    public static final Card queenOfHearts = new Card(Suits.HEARTS, new Integer(1), "Queen", 11);
    public static final Card kingOfHearts = new Card(Suits.HEARTS, new Integer(1), "King", 12);
    public static final Card aceOfHearts = new Card(Suits.HEARTS, new Integer(1), "Ace", 13);

    public static final Card twoOfSpades = new Card(Suits.SPADES, new Integer(0), "2", 1);
    public static final Card threeOfSpades = new Card(Suits.SPADES, new Integer(0), "3", 2);
    public static final Card fourOfSpades = new Card(Suits.SPADES, new Integer(0), "4", 3);
    public static final Card fiveOfSpades = new Card(Suits.SPADES, new Integer(0), "5", 4);
    public static final Card sixOfSpades = new Card(Suits.SPADES, new Integer(0), "6", 5);
    public static final Card sevenOfSpades = new Card(Suits.SPADES, new Integer(0), "7", 6);
    public static final Card eightOfSpades = new Card(Suits.SPADES, new Integer(0), "8", 7);
    public static final Card nineOfSpades = new Card(Suits.SPADES, new Integer(0), "9", 8);
    public static final Card tenOfSpades = new Card(Suits.SPADES, new Integer(0), "10", 9);
    public static final Card jackOfSpades = new Card(Suits.SPADES, new Integer(0), "Jack", 10);
    public static final Card queenOfSpades = new Card(Suits.SPADES, new Integer(13), "Queen", 11);
    public static final Card kingOfSpades = new Card(Suits.SPADES, new Integer(0), "King", 12);
    public static final Card aceOfSpades = new Card(Suits.SPADES, new Integer(0), "Ace", 13);

    private Card[] deck = new Card[52];
    private int pointer = 0;
    private final int FULL = 52;



    /**
     * It intializes a  DeckOfCards object which consist of all distinct Diamonds, Clubs, Hearts,
     * Spades suit cards
     * <p/>
     */
    public HeartsDeckOfCards() {

        initDeck();
    }

    /**
     * Removes a card from the deck and returns the  card
     * <p/>
     * <dt>precondition: A DeckOfCards object has been instantiated.
     *
     * @returns: A Cards representation of card is return.
     */
    public Card getCard() throws EmptyDeckException {

        if (pointer == FULL) {
            throw new EmptyDeckException("Deck is empty");
        }

        Card card = deck[pointer];
        pointer++;
        return card;
    }


    /**
     * Checks if the deck of cards is empty.
     *
     * @returns
     */
    public boolean isEmpty() {
        return pointer == FULL;
    }


    /**
     *
     */
    public void initDeck() {

        LinkedList<Card> deck = new LinkedList<>();
        deck.add(twoOfClubs);
        deck.add(threeOfClubs);
        deck.add(fourOfClubs);
        deck.add(fiveOfClubs);
        deck.add(sixOfClubs);
        deck.add(sevenOfClubs);
        deck.add(eightOfClubs);
        deck.add(nineOfClubs);
        deck.add(tenOfClubs);
        deck.add(jackOfClubs);
        deck.add(queenOfClubs);
        deck.add(kingOfClubs);
        deck.add(aceOfClubs);

        deck.add(twoOfHearts);
        deck.add(threeOfHearts);
        deck.add(fourOfHearts);
        deck.add(fiveOfHearts);
        deck.add(sixOfHearts);
        deck.add(sevenOfHearts);
        deck.add(eightOfHearts);
        deck.add(nineOfHearts);
        deck.add(tenOfHearts);
        deck.add(jackOfHearts);
        deck.add(queenOfHearts);
        deck.add(kingOfHearts);
        deck.add(aceOfHearts);

        deck.add(twoOfDiamonds);
        deck.add(threeOfDiamonds);
        deck.add(fourOfDiamonds);
        deck.add(fiveOfDiamonds);
        deck.add(sixOfDiamonds);
        deck.add(sevenOfDiamonds);
        deck.add(eightOfDiamonds);
        deck.add(nineOfDiamonds);
        deck.add(tenOfDiamonds);
        deck.add(jackOfDiamonds);
        deck.add(queenOfDiamonds);
        deck.add(kingOfDiamonds);
        deck.add(aceOfDiamonds);

        deck.add(twoOfSpades);
        deck.add(threeOfSpades);
        deck.add(fourOfSpades);
        deck.add(fiveOfSpades);
        deck.add(sixOfSpades);
        deck.add(sevenOfSpades);
        deck.add(eightOfSpades);
        deck.add(nineOfSpades);
        deck.add(tenOfSpades);
        deck.add(jackOfSpades);
        deck.add(queenOfSpades);
        deck.add(kingOfSpades);
        deck.add(aceOfSpades);

        boolean usedVals[] = new boolean[52];

        while (deck.size() > 0) {

            Random r = new Random();
            int index = r.nextInt(52);

            if (usedVals[index] == false) {
                this.deck[index] = deck.removeFirst();
                usedVals[index] = true;
            }

        }
    }

    public static void main(String arg[]){

        HeartsDeckOfCards dc = new HeartsDeckOfCards();
        dc.initDeck();

        ArrayList<Card<Suits, Integer>> list = new ArrayList<>();
       while(!dc.isEmpty()){
           try {

               list.add(dc.getCard());
           } catch (EmptyDeckException e) {
               e.printStackTrace();
           }
       }


       System.out.println();


        Collections.sort(list, new Comparator<Card<Suits, Integer>>(){
            @Override
            public int compare(Card<Suits, Integer> o1, Card<Suits, Integer> o2) {
                if(o1.getSuit()==o2.getSuit()){
                    if(o1.getOrdinal()>o2.getOrdinal()) {
                        return 1;
                    }else{
                        return -1;
                    }
                }else if(o1.getSuit() == Suits.HEARTS &&  o2.getSuit()!=Suits.HEARTS){
                    return 1;
                }else if( o1.getSuit()!=Suits.HEARTS && o2.getSuit()==Suits.HEARTS){
                    return -1;
                }else if(o1.getSuit() == Suits.HEARTS &&
                        (o2.getSuit()== Suits.DIAMONDS ||
                                o2.getSuit()== Suits.SPADES)){
                    return 1;
                }else if((o1.getSuit() == Suits.DIAMONDS || o1.getSuit() == Suits.SPADES) && o2.getSuit()==Suits.CLUBS){
                    return -1;
                }else if(o1.getSuit() == Suits.SPADES  && o2.getSuit()==Suits.DIAMONDS){
                    return 1;
                }else if(o1.getSuit()!=Suits.DIAMONDS && o2.getSuit() == Suits.SPADES){
                    return -1;
                }
                return -1;
            }
        });

        for(Card c : list){

            System.out.println(c);
        }

    }

}
