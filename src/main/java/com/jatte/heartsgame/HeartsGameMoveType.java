package com.jatte.heartsgame;


import com.jatte.client.GameMoves;

/**
 * Created by jovaughnlockridge1 on 9/12/16.
 */
public enum HeartsGameMoveType implements GameMoves {
        DEALING, PASS_LEFT, PASS_RIGHT, PASS_ACROSS, PLAY_HANDS, END_GAME
}
