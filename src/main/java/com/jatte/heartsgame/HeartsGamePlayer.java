package com.jatte.heartsgame;

import com.jatte.api.cards.Card;
import com.jatte.api.cards.Suits;
import com.jatte.client.GamePlayer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by jovaughnlockridge1 on 9/16/16.
 */
public class HeartsGamePlayer extends GamePlayer implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(HeartsGamePlayer.class);
    private final String userName;
    private List<Card<Suits, Integer>> playerCards = new ArrayList<>();
    private boolean hasMoved = false;

    public HeartsGamePlayer(String userName){
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public List<Card<Suits, Integer>> getPlayerCards() {
        return playerCards;
    }

    public final void addPlayerCard(Card card) {
        playerCards.add(card);
    }

    public final void addPlayerCards(List<Card<Suits, Integer>> cards) {
        for(Card card : cards){
            addPlayerCard(card);
        }
    }

    public final void removePlayerCards(List<Card<Suits, Integer>> removeCards) {
        for(Card removeCard : removeCards){
            if(!removePlayerCard(removeCard)){
                LOGGER.error("could not remove card {} ",removeCard);
            }
        }
    }


    public final boolean  removePlayerCard(Card card){
        return playerCards.remove(card);
    }

    public final void removeAllPlayerCards() {
        playerCards.clear();
    }

    public boolean hasMoved() {
        return hasMoved;
    }

    public void setHasMoved(boolean hasMoved) {
        this.hasMoved = hasMoved;
    }

    public Iterable<Card<Suits, Integer>> playerCards() {
        return playerCards;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o.getClass()  == HeartsGamePlayer.class)) return false;
        HeartsGamePlayer that = (HeartsGamePlayer) o;
        return Objects.equals(this.userName, that.userName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userName);
    }
}
