package com.jatte.heartsgame.States;

import com.jatte.IllegalMoveException;
import com.jatte.PlayerMove;
import com.jatte.api.cards.Card;
import com.jatte.api.cards.Suits;
import com.jatte.client.GameStateUpdate;
import com.jatte.heartsgame.HeartsGame;
import com.jatte.heartsgame.HeartsGameMoveType;
import com.jatte.heartsgame.HeartsGamePlayer;
import com.jatte.heartsgame.HeartsDeckOfCards;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.*;

/**
 * Created by Jovaughn Lockridge on 10/12/2016.
 */
public class PlayHands extends HeartsGameState implements Serializable {

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(PlayHands.class);
    private boolean hasTwoOfClubsBeenPlayed;
    private Suits leadingSuit = null;
    private boolean iceBroken;
    private final Card twoOfClubs = new Card(Suits.CLUBS, new Integer(0), "2");

    public PlayHands(HeartsGame game) {
        super(game);
    }

    public void startRound() {
        LOGGER.info("Starting Round from PlayingHands state");
        hasTwoOfClubsBeenPlayed = false;
        leadingSuit = null;
        iceBroken = false;
        Collection<HeartsGamePlayer> players = game.getGamePlayers();
        HeartsGamePlayer playerWith2OfClubs = getPlayerWithTwoOfClubs(players);
        game.setGameMessage("Player " + playerWith2OfClubs.getUserName() + " will start with 2 of Clubs.");
        game.setWhoMove(playerWith2OfClubs);
    }


    @Override
    public GameStateUpdate move(PlayerMove move)  {

        GameStateUpdate update = null;

        String userName = move.getPlayer();

        HeartsGamePlayer player = game.getPlayer(userName);
        if (player.hasMoved()) {
//            throw new IllegalMoveException("Player " + player.getUserName() + " have moved already .");
        }

        Card<Suits, Integer> cardMove = move.getMove().get(0);
        try {
            validatePlayedCard(cardMove, player);
        } catch (IllegalMoveException e) {
            e.printStackTrace();
        }

        LOGGER.info("Player {} played {} ", player.getUserName(), cardMove);
        if (leadingSuit == null) {
            leadingSuit = cardMove.getSuit();
        }

        player.removePlayerCard(cardMove);
        game.playCard(userName, cardMove);
        player.setHasMoved(true);

        if (game.isEndOfBook()) {
            game.setIsPostMoveUpdate(true);
        } else {
            Integer id = game.getPlayerId(userName);
            if (id == 4) {
                game.setWhoMove(game.getGamePlayerByPlayerId(1));
            } else {
                game.setWhoMove(game.getGamePlayerByPlayerId(id + 1));
            }
        }
        LOGGER.info("{} has these cards left in hand {} ", player.getUserName(), player.getPlayerCards());
        String playerTurn = game.getWhoMove().getUserName();
        game.setGameMessage("It's " + game.getWhoMove().getUserName() + " move.");
        LOGGER.info("It is {} move in game {} ", playerTurn, game.getName());
        return update;
    }

    @Override
    public void postMove() {
        Map<String, Card<Suits, Integer>> moves = game.getBoardTable();
        String bookLeader = null;
        Integer highestBookValue = -1;
        List<Card<Suits, Integer>> penaltyBooks = new ArrayList<>(4);
        for (Map.Entry<String, Card<Suits, Integer>> entry : moves.entrySet()) {
            Card<Suits, Integer> c = entry.getValue();
            String playerName = entry.getKey();

            if (c.getSuit() == leadingSuit && c.getOrdinal().compareTo(highestBookValue) > 0) {
                bookLeader = playerName;
                highestBookValue = c.getOrdinal();
            }

            if (!iceBroken && (c.getSuit() == Suits.HEARTS || c.equals(HeartsDeckOfCards.queenOfSpades))) {
                iceBroken = true;
                LOGGER.info("Ice has been broken for round.");
            }

            penaltyBooks.add(c);
        }

        LOGGER.info("Player {} won this round of books {} ", bookLeader, penaltyBooks);
        game.persistRoundBooks(bookLeader, penaltyBooks);

        for (HeartsGamePlayer p : game.getGamePlayers()) {
            p.setHasMoved(false);
            if (p.getUserName().equals(bookLeader)) {
                game.setWhoMove(p);
            }
        }
        leadingSuit = null;
        game.clearPlayedCards();
        game.setIsPostMoveUpdate(false);

        if (game.isEndOfRound()) {
            LOGGER.info("End of round calculating scores.");
            calculateEndOfRoundScores();
            game.clearRoundBooks();
            if (game.isGameOver()) {
                LOGGER.info("Game is over and the winner is {} ", game.getGameWinner());
                game.setGameState(game.END_GAME);
            } else {
                game.setGameState(game.DEALING);
            }
        } else {
            game.setGameMessage("It's " + game.getWhoMove().getUserName() + " move.");
        }
    }


    public void validatePlayedCard(Card<Suits, Integer> card, HeartsGamePlayer player) throws IllegalMoveException {

        if (!player.equals(game.getWhoMove())) {
            throw new IllegalMoveException("It is not player " + player.getUserName() + " turn. ");
        }

        boolean hasLeadingSuitInHand = false;
        boolean hasNonHeartSuit = false;
        for (Card c : player.getPlayerCards()) {
            if (c.getSuit() == leadingSuit) {
                hasLeadingSuitInHand = true;
            }

            if (c.getSuit() != Suits.HEARTS) {
                hasNonHeartSuit = true;
            }
        }

        if (leadingSuit != null && card.getSuit() != leadingSuit && hasLeadingSuitInHand) {
            throw new IllegalMoveException("Leading suit " + leadingSuit + " does not match played suit " + card.getSuit());
        }

        if (!hasTwoOfClubsBeenPlayed && !card.equals(twoOfClubs)) {
            throw new IllegalMoveException("First move must be 2 of Clubs");
        }

        if (!hasTwoOfClubsBeenPlayed && card.equals(twoOfClubs)) {
            hasTwoOfClubsBeenPlayed = true;
        }

        if (game.isFirstPlayedCardOfBook() && card.getSuit() == Suits.HEARTS && !iceBroken && hasNonHeartSuit) {
            throw new IllegalMoveException("Can't play hearts ice has not been broken.");
        }

        if (leadingSuit != card.getSuit() && card.getSuit() == Suits.HEARTS && !iceBroken && hasLeadingSuitInHand) {
            throw new IllegalMoveException("Can't play hearts ice has not been broken.");
        }
    }

    @Override
    public HeartsGameMoveType getHeartGameMoveType() {
        return HeartsGameMoveType.PLAY_HANDS;
    }

    @Override
    public String toString() {
        return "PlayHands";
    }

    public void calculateEndOfRoundScores() {

        Integer playerPenaltyBooks = 0;
        HashSet<Integer> tiedScores = new HashSet<>();
        for (HeartsGamePlayer p : game.getGamePlayers()) {

            List<Card<Suits, Integer>> cards = game.getRoundBooks(p.getUserName());
            if (cards != null) {
                playerPenaltyBooks = getPenaltyPoints(cards);

                if (playerPenaltyBooks == 26) {
                    LOGGER.info("Shooting moon recalculating scores ");
                    p.setPlayerScore(0);
                    for (HeartsGamePlayer penaltyPlayer : game.getGamePlayers()) {
                        if (!p.getUserName().equals(penaltyPlayer.getUserName())) {
                            penaltyPlayer.addPlayerScore(26);
                            LOGGER.info("Player {} has {} points. ", penaltyPlayer.getUserName(), penaltyPlayer.getPlayerScore());
                        }
                    }
                } else if (playerPenaltyBooks > 0) {
                    p.addPlayerScore(playerPenaltyBooks);

                }
            }
            LOGGER.info("Player {} has {} points. ", p.getUserName(), p.getPlayerScore());
            tiedScores.add(p.getPlayerScore());
        }

        HeartsGamePlayer gameWinner = getGameWinner();
        if (tiedScores.size() == 4) {
            if (game.isGameOver()) {
                game.setGameWinner(gameWinner);
            }
        } else if (tiedScores.size() < 4) {
            if (game.isGameOver()) {
                game.setGameWinner(null);
                game.setGameOver(false);
            }
        }
    }

    private Integer getPenaltyPoints(List<Card<Suits, Integer>> cards) {
        if (cards == null || cards.isEmpty()) {
            return 0;
        }

        Integer playerPenaltyBooks = 0;
        for (Card<Suits, Integer> card : cards) {
            if (card.getSuit() == Suits.HEARTS || (card.getFace().equals("Queen") && card.getSuit() == Suits.SPADES)) {
                playerPenaltyBooks += card.getCardValue();
            }
        }
        return playerPenaltyBooks;
    }


    public HeartsGamePlayer getGameWinner() {
        HeartsGamePlayer gameWinner = null;
        for (HeartsGamePlayer p : game.getGamePlayers()) {
            if (gameWinner == null || p.getPlayerScore() < gameWinner.getPlayerScore()) {
                gameWinner = p;
            }

            if (p.getPlayerScore() >= game.getEndOfGameScore()) {
                game.setGameOver(true);
            }
        }
        return gameWinner;
    }

    private HeartsGamePlayer getPlayerWithTwoOfClubs(Collection<HeartsGamePlayer> players) {
        HeartsGamePlayer playerWith2OfClubs = null;
        for (HeartsGamePlayer player : players) {

            player.setHasMoved(false);
            if (playerWith2OfClubs != null) {
                continue;
            }
            for (Card card : player.getPlayerCards()) {

                if (card.equals(new Card(Suits.CLUBS, new Integer(0), "2"))) {
                    playerWith2OfClubs = player;
                    break;
                }
            }
        }
        return playerWith2OfClubs;
    }


}
