package com.jatte.heartsgame.States;

import com.jatte.PlayerMove;
import com.jatte.client.GameStateUpdate;
import com.jatte.heartsgame.HeartsGame;
import com.jatte.heartsgame.HeartsGameMoveType;

import java.io.Serializable;

/**
 * Created by jovaughnlockridge1 on 11/7/16.
 */
public class EndOfGame extends HeartsGameState implements Serializable {

    public EndOfGame(HeartsGame game){
       super(game);
    }

    @Override
    public GameStateUpdate move(PlayerMove move) {return null;}

    @Override
    public HeartsGameMoveType getHeartGameMoveType() {
        return HeartsGameMoveType.END_GAME;
    }

    @Override
    public String toString() {
        return "EndOfGame{" +
                "game=" + game +
                '}';
    }
}
