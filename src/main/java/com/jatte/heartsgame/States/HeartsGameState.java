package com.jatte.heartsgame.States;

import com.jatte.IllegalMoveException;
import com.jatte.PlayerMove;
import com.jatte.client.GameStateUpdate;
import com.jatte.heartsgame.HeartsGame;
import com.jatte.heartsgame.HeartsGameMoveType;


import java.io.Serializable;

/**
 * Created by jovaughnlockridge1 on 9/14/16.
 */
public abstract class HeartsGameState implements Serializable {

    protected final HeartsGame game;

    public HeartsGameState(HeartsGame game){
        this.game = game;
    }

    public GameStateUpdate dealingComplete(){ return null;}

    public void startRound(){}

    public abstract GameStateUpdate move(PlayerMove move);

    public void postMove() throws IllegalMoveException { /** noop */ }

    public abstract HeartsGameMoveType getHeartGameMoveType();
}
