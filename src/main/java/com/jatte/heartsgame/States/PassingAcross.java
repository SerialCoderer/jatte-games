package com.jatte.heartsgame.States;

import com.jatte.heartsgame.HeartsGame;
import com.jatte.heartsgame.HeartsGameMoveType;
import com.jatte.heartsgame.HeartsGamePlayer;

import java.io.Serializable;

/**
 * PassingLeft consist of passing cards in a clockwise fashion.
 * Player 1 -> Player 3
 * Player 2 -> Player 4
 * Player 3 -> Player 1
 * Player 4 -> Player 2
 * <p>
 * <p>
 * Created by Jovaughn Lockridge on 11/7/16.
 */
public class PassingAcross extends AbstractPassing implements Serializable {

    public PassingAcross(HeartsGame game) {
        super(game);
    }


    @Override
    public HeartsGamePlayer getReceivingLeftPassPlayer(Integer passingLeftPlayerId) {
        if (passingLeftPlayerId == 4) {
            return game.getGamePlayerByPlayerId(2);
        } else if (passingLeftPlayerId == 3) {
            return game.getGamePlayerByPlayerId(1);
        }
        return game.getGamePlayerByPlayerId(passingLeftPlayerId + 2);
    }

    @Override
    public HeartsGameMoveType getHeartGameMoveType() {
        return HeartsGameMoveType.PASS_ACROSS;
    }

    @Override
    public String toString() {
        return "PassingAcross{" +
                "game=" + game +
                '}';
    }
}
