package com.jatte.heartsgame.States;

import com.jatte.PlayerMove;
import com.jatte.api.cards.Card;
import com.jatte.api.cards.Suits;
import com.jatte.client.GameStateUpdate;
import com.jatte.heartsgame.HeartsGame;
import com.jatte.heartsgame.HeartsGamePlayer;
import com.jatte.service.serverRequest.ServerActions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * Created by jovaughnlockridge1 on 11/6/16.
 */
public abstract class AbstractPassing extends HeartsGameState implements Serializable {

    public AbstractPassing(HeartsGame game) {
        super(game);
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractPassing.class);

    @Override
    public GameStateUpdate move(PlayerMove move) {

        String userName = move.getPlayer();
        LOGGER.info("Received move {} from user {} ", move, userName);
        HeartsGamePlayer passingLeftPlayer = game.getPlayer(userName);

        List<Card<Suits, Integer>> cardMoves = move.getMove();
        if (passingLeftPlayer == null || passingLeftPlayer.hasMoved() || cardMoves == null || cardMoves.size() != 3) {
//            throw new IllegalMoveException("Player "+userName+" has moved");
        }

        passingLeftPlayer.removePlayerCards(cardMoves);
        Integer passingLeftPLayerId = game.getPlayerId(userName);
        HeartsGamePlayer receivingLeftPassPlayer = getReceivingLeftPassPlayer(passingLeftPLayerId);

        if (receivingLeftPassPlayer.hasMoved()) {
            receivingLeftPassPlayer.addPlayerCards(cardMoves);
        } else {
            game.setCachedCards(receivingLeftPassPlayer.getUserName(), cardMoves);
        }

        LOGGER.info("Player {} is passing cards={} to {} ", userName, cardMoves, receivingLeftPassPlayer.getUserName());
        List<Card<Suits, Integer>> cachedCards = game.getCachedCards(passingLeftPlayer.getUserName());
        if (cachedCards != null && !cachedCards.isEmpty()) {
            passingLeftPlayer.addPlayerCards(cachedCards);
            cachedCards.clear();
        }

        passingLeftPlayer.setHasMoved(true);

        Collection<HeartsGamePlayer> players = game.getGamePlayers();
        boolean stateChange = players.stream().allMatch(p -> p.hasMoved());
        game.setGameMessage("Player " + passingLeftPlayer.getUserName() + " has passed left.");

        if(stateChange) {
            LOGGER.info("Changing game state to {} ", game.PLAY_HANDS);
            game.setGameState(game.PLAY_HANDS);
        }

//        GameStateUpdate gameStateUpdate = new GameStateUpdate(ServerActions.UPDATE_SCREEN.name(), )

        return null;
    }


    public abstract HeartsGamePlayer getReceivingLeftPassPlayer(Integer passingLeftPLayerId);
}
