package com.jatte.heartsgame.States;

import com.jatte.PlayerMove;
import com.jatte.client.GameStateUpdate;
import com.jatte.heartsgame.HeartsGame;
import com.jatte.heartsgame.HeartsGameMoveType;
import com.jatte.heartsgame.HeartsGamePlayer;

import java.io.Serializable;

/**
 * PassingLeft consist of passing cards in a clockwise fashion.
 * Player 1 -> Player 4
 * Player 2 -> Player 1
 * Player 3 -> Player 2
 * Player 4 -> Player 3
 * <p>
 * Created by Jovaughn Lockridge on 11/6/16.
 */
public class PassingRight extends AbstractPassing implements Serializable {

    public PassingRight(HeartsGame game) {
        super(game);
    }

    @Override
    public HeartsGamePlayer getReceivingLeftPassPlayer(Integer passingLeftPlayerId) {
        if (passingLeftPlayerId == 1) {
            return game.getGamePlayerByPlayerId(4);
        }

        return game.getGamePlayerByPlayerId(passingLeftPlayerId - 1);
    }

    @Override
    public HeartsGameMoveType getHeartGameMoveType() {
        return HeartsGameMoveType.PASS_RIGHT;
    }

    @Override
    public String toString() {
        return "PassingRight{" +
                "game=" + game +
                '}';
    }
}
