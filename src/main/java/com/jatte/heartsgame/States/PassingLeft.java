package com.jatte.heartsgame.States;

import com.jatte.PlayerMove;
import com.jatte.client.GameStateUpdate;
import com.jatte.heartsgame.HeartsGame;
import com.jatte.heartsgame.HeartsGameMoveType;
import com.jatte.heartsgame.HeartsGamePlayer;

import java.io.Serializable;

/**
 * PassingLeft consist of passing cards in a clockwise fashion.
 * Player 1 -> Player 2
 * Player 2 -> Player 3
 * Player 3 -> Player 4
 * Player 4 -> Player 1
 * <p>
 * <p>
 * Created by Jovaughn Lockridge on 9/14/16.
 */
public class PassingLeft extends AbstractPassing implements Serializable {

    public PassingLeft(HeartsGame game) {
        super(game);
    }

    @Override
    public HeartsGamePlayer getReceivingLeftPassPlayer(Integer passingLeftPlayerId) {
        if (passingLeftPlayerId == 4) {
            return game.getGamePlayerByPlayerId(1);
        }
        return game.getGamePlayerByPlayerId(passingLeftPlayerId + 1);
    }

    @Override
    public HeartsGameMoveType getHeartGameMoveType() {
        return HeartsGameMoveType.PASS_LEFT;
    }

    @Override
    public String toString() {
        return "PassingLeft{" +
                "game=" + game +
                '}';
    }
}
