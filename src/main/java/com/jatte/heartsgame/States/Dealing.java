package com.jatte.heartsgame.States;


import com.jatte.IllegalMoveException;
import com.jatte.PlayerMove;
import com.jatte.api.cards.Card;
import com.jatte.api.cards.Suits;
import com.jatte.client.GameStateUpdate;
import com.jatte.heartsgame.HeartsGame;
import com.jatte.heartsgame.HeartsGameMoveType;

import java.io.Serializable;
import java.util.List;

/**
 * Created by jovaughnlockridge1 on 9/14/16.
 */
public class Dealing extends HeartsGameState implements Serializable {

    public Dealing(HeartsGame game){
        super(game);
    }

    @Override
    public GameStateUpdate dealingComplete(){
        GameStateUpdate update = null;
        int round = game.getRoundsPlayed()%4;
        switch(round){
            case 0 :
                game.setGameState(game.PLAY_HANDS);
                break;
            case 1 : game.setGameState(game.PASSING_LEFT);
                game.setGameMessage("Select three cards to pass to your left.");
                break;
            case 2 : game.setGameState(game.PASSING_RIGHT);
                game.setGameMessage("Select three cards to pass to your right.");
                break;
            case 3 : game.setGameState(game.PASSING_ACROSS);
                game.setGameMessage("Select three cards to pass to across.");
                break;
        }
        return update;
    }


    @Override
    public HeartsGameMoveType getHeartGameMoveType() {
        return HeartsGameMoveType.DEALING;
    }

    @Override
    public GameStateUpdate move(PlayerMove move) {return null;}

    @Override
    public String toString() {
        return "Dealing{" +
                "game=" + game +
                '}';
    }
}
