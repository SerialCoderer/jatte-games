package com.jatte.heartsgame;

import com.jatte.BoardGame;
import com.jatte.GameKey;
import com.jatte.IllegalMoveException;
import com.jatte.PlayerMove;
import com.jatte.api.cards.Card;
import com.jatte.api.cards.EmptyDeckException;
import com.jatte.api.cards.Suits;
import com.jatte.client.GameStateUpdate;
import com.jatte.heartsgame.States.*;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.*;


public class HeartsGame extends BoardGame<PlayerMove, HeartsGamePlayer> implements Serializable{

    private HeartsDeckOfCards heartsDeck = new HeartsDeckOfCards();
    private boolean gameOver = false;
    public final HeartsGameState DEALING = new Dealing(this);
    public final HeartsGameState PASSING_LEFT = new PassingLeft(this);
    public final HeartsGameState PASSING_RIGHT = new PassingRight(this);
    public final HeartsGameState PLAY_HANDS = new PlayHands(this);
    public final HeartsGameState PASSING_ACROSS = new PassingAcross(this);
    public final HeartsGameState END_GAME = new EndOfGame(this);
    private HeartsGameState gameState = null;
    private Map<String, List<Card<Suits, Integer>>> moveCache = new HashMap<>();
    private Map<String, Card<Suits, Integer>> bookMoves = new HashMap<>();
    private Map<String, List<Card<Suits, Integer>>> roundMove = new HashMap<>();
    private HeartsGamePlayer gameWinner = null;

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(HeartsGame.class);
    private int endOfGameScore = 0;

    public HeartsGame() {
        super();
        setCurrentRoundNumber(1);
        setEndOfGameScore(100);
    }

    public String getName() {
        return "HEARTS";
    }

    public HeartsGame(GameKey gameKey) {
        super(gameKey);
    }

    public GameStateUpdate startGame() {
        GameStateUpdate update = null;
        try {
            LOGGER.info("Starting Hearts game");
            update =  dealMove();
        } catch (EmptyDeckException e) {
            e.printStackTrace();
        }
        return update;
    }

    private GameStateUpdate dealMove() throws EmptyDeckException {
        GameStateUpdate update = null;
        if (isFull()) {
            int playerId = 1;
            while (!heartsDeck.isEmpty()) {
                int x = 13;
                while (x > 0) {
                    Card card = heartsDeck.getCard();
                    HeartsGamePlayer player = getGamePlayerByPlayerId(playerId);
                    player.addPlayerCard(card);
                    LOGGER.info("Player={} was dealt card={}  ", player.getUserName(), card);
                    x--;
                }
                playerId++;
            }
            setGameState(DEALING);
            gameState.dealingComplete();
            update = new GameStateUpdate(gameState.getHeartGameMoveType().name(), getGamePlayersMap());
        }
        return update;
    }


    public boolean isGameOver() {
        return gameOver;
    }

    public void endGame() {
    }

    @Override
    public GameStateUpdate move(PlayerMove move) {
        return gameState.move(move);
    }

    @Override
    public String toString() {
        return "Hearts Game";
    }

    @Override
    public boolean isFull() {
        return getGamePlayersMap().values().size() == 4;
    }

    public HeartsGameState getGameState() {
        return this.gameState;
    }

    public void setGameState(HeartsGameState gameState) {
        LOGGER.info("GameState is now {} ", gameState);
        this.gameState = gameState;
        this.gameState.startRound();
        notifyGameStatusChange();
    }

    @Override
    public void addPlayer(String userName) {
        HeartsGamePlayer player = new HeartsGamePlayer(userName);
        super.addPlayer(player);
    }


    public List<Card<Suits, Integer>> getCachedCards(String playerName) {
        return moveCache.get(playerName);
    }

    public void setCachedCards(String playerName, List<Card<Suits, Integer>> cachedCards) {
        moveCache.put(playerName, cachedCards);
    }


    public void playCard(String playerName, Card card) {
        bookMoves.put(playerName, card);
    }

    public boolean isEndOfBook() {
        return bookMoves.values().size() == 4;
    }

    public boolean isEndOfRound() {
        Collection<HeartsGamePlayer> players = getGamePlayers();
        for (HeartsGamePlayer player : players) {
            if (!player.getPlayerCards().isEmpty()) {
                return false;
            }
        }
        return true;
    }

    public boolean isFirstPlayedCardOfBook() {
        return bookMoves.values().size() == 0;
    }

    public void clearPlayedCards() {
        bookMoves.clear();
    }


    public void persistRoundBooks(String playerName, List<Card<Suits, Integer>> card) {
        List<Card<Suits, Integer>> playerRoundMoves = roundMove.get(playerName);
        if (roundMove.get(playerName) == null) {
            playerRoundMoves = new ArrayList<>(13);
            roundMove.put(playerName, playerRoundMoves);
        }
        playerRoundMoves.addAll(card);
    }

    public List<Card<Suits, Integer>> getRoundBooks(String playerName) {
        return roundMove.get(playerName);
    }

    public void clearRoundBooks() {
        roundMove.clear();
    }

    @Override
    public void postMove(){
        try {
            gameState.postMove();
        } catch (IllegalMoveException e) {
            e.printStackTrace();
        }
        if (isEndOfRound() && !isGameOver()) {
            incrementRoundsPlayed();
            heartsDeck = new HeartsDeckOfCards();
            try {
                dealMove();
            } catch (EmptyDeckException ede) {
//                throw new IllegalMoveException("Something went wrong ");
            }
        }
    }

    public void setEndOfGameScore(int endOFGameScore) {
        this.endOfGameScore = endOFGameScore;
    }

    public int getEndOfGameScore() {
        return endOfGameScore;
    }

    public void setGameOver(boolean isEndOfGame) {
        this.gameOver = isEndOfGame;
    }

    public void setGameWinner(HeartsGamePlayer heartsGameWinner) {
        this.gameWinner = heartsGameWinner;
    }

    /**
     * Return the player with the lowest score when the game state is End_GAME
     * otherwise this will return null.
     *
     * @return player with lowest score
     */
    public HeartsGamePlayer getGameWinner() {
        return gameWinner;
    }

//    @Override
//    public GameStateUpdate move(PlayerMove playerMove) {
////        if (move.getMoveType() != gameState.getHeartGameMoveType()) {
////            throw new IllegalMoveException("Game State does not match current game state.");
////            ///TODO: refresh client's stale game state
////        }
//        return gameState.move(playerMove);
//    }

    @Override
    public Map<String, Card<Suits, Integer>> getBoardTable() {
        return bookMoves;
    }

}
